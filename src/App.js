import React, {useState, useEffect} from 'react';
import {Form} from './Form'
import {List} from './List';

let storageData = []
if(localStorage.getItem('notebook')) {
  storageData = JSON.parse(localStorage.getItem('notebook'))
}

function App() {
  const [list, setList] = useState(storageData)
  useEffect(() => {
    localStorage.setItem('notebook', JSON.stringify(list))
  }, [list])
  return (
    <div>
      <Form onSubmit={(data) => {
        setList([...list, data])
      }}/>
      <List data={list}/>
    </div>
  );
}

export default App;

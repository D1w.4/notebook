import React from 'react';

export class List extends React.Component {
  render() {
    const result = this.props.data.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.firstName}</td>
          <td>{item.lastName}</td>
          <td>{item.description}</td>
        </tr>
      );
    })
    return (
      <table>
        <thead>
        <tr>
          <th>Имя</th>
          <th>Фамилия</th>
          <th>Описание</th>
        </tr>
        </thead>
        <tbody>
        {result}
        </tbody>
      </table>
    )
  }
}

import React from 'react';


export class Form extends React.Component {
  state = {
    firstName: '',
    lastName: '',
    description: ''
  }
  onSubmit = (event) => {
    this.props.onSubmit(this.state)
    this.setState({
      firstName: '',
      lastName: '',
      description: ''
    })
    event.preventDefault()
  }
  onChange = (event) => {
    const fieldKey = event.currentTarget.name
    const value = event.currentTarget.value
    this.setState({
      [fieldKey]: value
    })
  }
  render() {
    const {firstName, lastName, description} = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        <div>
          <label>Имя</label><br/>
          <input value={firstName} onChange={this.onChange} type="text" name="firstName"/>
        </div>
        <div>
          <label>Фамилия</label><br/>
          <input value={lastName} onChange={this.onChange} type="text" name="lastName"/>
        </div>
        <div>
          <label>Описание</label><br/>
          <textarea value={description} onChange={this.onChange} name="description" cols="30" rows="10"></textarea>
        </div>
        <div>
          <button type="submit">Отправить</button>
        </div>
      </form>
    )
  }
}
